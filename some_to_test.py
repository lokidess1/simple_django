import random
from re import findall


spell_list = [
    'fireball',
    'lightning_bolt',
    'frost_nova',
    'ice_bolt',
    'shadow_arrow'
]


class Player:
    hp = 100
    mp = 20
    skills = set()

    def __init__(self):
        # SOME WIERD LOGIC OF PLAYER CREATION
        pass


def some_other_method(factor):
    if factor > 50:
        return True
    else:
        return False


def obtain_random_skill(player):
    factor = random.randint(0, 100)
    if some_other_method(factor):
        player.skills.add(random.choices(spell_list)[0])
    else:
        raise ValueError


def some(filename):

    with open(filename, 'r') as file:
        nums = findall('\d+', file.read())
        return nums


def splitter(data):
    return data.split("")
