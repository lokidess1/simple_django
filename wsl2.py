import skills
import random


class Player:
    name = ''
    hp = 100
    mp = 100
    available_skills = set()

    def __init__(self, name):
        self.name = name


def travel(player):
    new_skill = random.choice(
        [x for x in dir(skills) if not x.startswith('__')]
    )
    player.available_skills.add(new_skill)


def fight(player):
    while True:
        skill = input("Select skill: ", )
        skill = getattr(skills, skill)
        player.mp -= skill.manacost


def end_game(*args):
    exit(0)


def show_skills(player):
    print(", ".join(player.available_skills))


# if __name__ == '__main__':
#     name = input('Enter your name: ')
#     player = Player(name=name)
#
#     actions = {
#         'travel': travel,
#         'fight': fight,
#         'exit': end_game,
#         'show_skills': show_skills
#     }
#
#     while True:
#         action = input('Choice action: ')
#         action_method = actions.get(action, None)
#         if not action_method:
#             print("Available Actions is: ", ', '.join(actions.keys()))
#             continue
#         action_method(player)


# try:
#     print("I try do something!")
# except ValueError:
#     print("Looks like ValueError Happens")
# except KeyError:
#     print("Looks like KeyError Happens")
# except ZeroDivisionError:
#     print("Looks like ZeroDivisionError Happens")
# else:
#     print("All other error!")
# finally:
#     print("Does not matter. I'll work any way!")


# for x in range(10):
#     print(x)
# else:
#     print("No break event happen!")


# a = 10
# b = 0
#
# try:
#     c = a / b
# except ZeroDivisionError:
#     pass
#
# if b != 0:
#     c = a / b


# try:
#     user = User.objects.get(username="Loki")
# except User.DoesNotExists:
#     pass
#
#
# user = User.objects.filter(username="Loki").first()
# if not user:
#     pass
