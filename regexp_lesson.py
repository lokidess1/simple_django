import re


my_text = "Hello My I Name * is 123 Loki! my Age 31 *"
russian_text = "Привет мир! Я алкаголик!"


# TODO match
# if re.match(r"[A-Z]", my_text):
#     print("Text is correct")
# else:
#     print("Text incorrect")


# TODO search
# regexp = re.search(r"[A-Z]", my_text)
# if regexp:
#     print("UPPER CASE IN TEXT", regexp.group(0))
# else:
#     print("NO UPPERCASE IN TEXT")

# TODO findall
# title_words = re.findall(r"[A-Z]\w*", my_text)
# print(title_words)

# TODO split
# regexp = re.split(r"\d+", my_text)
# print(regexp)

# TODO sub
# no_title_words = re.sub(r"[A-Z]\w*[ !]", '', my_text)
# print(no_title_words)

# TODO EXAMPLES
# float_str = "-123.1223"
# some_text = "qwe123 qweqew"
# print(re.findall('-?\d+\.?\d*', float_str))
# print(re.findall('[A-Za-z0-9]', my_text)) #  SAME AS \w
# print(re.findall('[^A-Za-z ]+', my_text))
# print(re.findall('[\*\+\-_]', my_text))
# print(re.findall('[а-я]+!', russian_text))
# print(re.findall('^-?\d+\.?\d*$', float_str))
# print(re.findall('\w{6}', some_text))
# print(re.findall('^q|a', some_text))
