FROM python:3.7-slim-stretch

RUN mkdir /app

WORKDIR /app

#COPY . /app
COPY . .

#RUN cp server_settings.py local_settings.py

RUN pip install -r requirements.txt
