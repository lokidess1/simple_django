import requests
import time
import os
import multiprocessing
import asyncio
import aiohttp
import aiofiles


def flat_solution():
    for counter in range(0, 100):
        response = requests.get('https://loremflickr.com/320/240', allow_redirects=True)  # , verify=False)
        if response.status_code == 200:
            with open(os.path.join('lorem_flicker_img', f'image_{counter}.jpg'), 'wb') as img:
                img.write(response.content)
        print(counter)


def multiprocessing_solution():

    def download(num, batch_size):
        for th in range(0, batch_size):
            response = requests.get('https://loremflickr.com/320/240', allow_redirects=True)  # , verify=False)
            if response.status_code == 200:
                with open(os.path.join('lorem_flicker_img', f'image_{num}_{th}.jpg'), 'wb') as img:
                    img.write(response.content)

    processes = []
    for counter in range(0, 1000):
        process = multiprocessing.Process(target=download, args=(counter, 1))
        process.start()
        processes.append(process)

    [x.join() for x in processes]


async def download(session, num):
    for counter in range(5):
        async with session.get('https://loremflickr.com/320/240') as response:
            async with aiofiles.open(os.path.join('lorem_flicker_img', f'image_{num}_{counter}.jpg'), 'wb') as img:
                await img.write(await response.read())


async def solution():
    tasks = []
    async with aiohttp.client.ClientSession() as session:
        for num in range(400):
            tasks.append(
                asyncio.ensure_future(download(session, num))
            )

        await asyncio.wait(tasks)


def async_solution():
    loop = asyncio.get_event_loop()
    loop.run_until_complete(solution())


if __name__ == '__main__':
    t1 = time.time()

    # flat_solution()
    # multiprocessing_solution()
    async_solution()

    print(time.time() - t1)
