from rest_framework.permissions import BasePermission


class TeacherOnly(BasePermission):

    def has_permission(self, request, view):
        if not request.user.is_authenticated:
            return False

        return request.user.groups.filter(name='Учитель').exists()
