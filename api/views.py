from rest_framework import generics, mixins
from rest_framework.viewsets import ModelViewSet, GenericViewSet
from core.models import Track, Author
from api.serializers import TrackSerializer, AuthorSerializer
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework import renderers
from api.permissions import TeacherOnly
from rest_framework.authentication import TokenAuthentication
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import SessionAuthentication, BasicAuthentication


class CsrfExemptSessionAuthentication(SessionAuthentication):

    def enforce_csrf(self, request):
        return  # To not perform the csrf check previously happening


@method_decorator(csrf_exempt, name='dispatch')
class TrackViewSet(ModelViewSet):
    queryset = Track.objects.all()
    serializer_class = TrackSerializer
    authentication_classes = [CsrfExemptSessionAuthentication, BasicAuthentication]
    # renderer_classes = [renderers.BrowsableAPIRenderer]

    # def get_serializer_class(self):

    # def get_queryset(self):
    #     pass


@method_decorator(csrf_exempt, name='dispatch')
class AuthorViewSet(mixins.ListModelMixin, mixins.CreateModelMixin, GenericViewSet):
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer
    permission_classes = [AllowAny]
    authentication_classes = [CsrfExemptSessionAuthentication, BasicAuthentication]

    # def get_permissions(self):
    #     pass

    # def get(self):
    # def post(self):

