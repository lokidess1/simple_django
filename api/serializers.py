from rest_framework import serializers
from core.models import Track, Author, Genre, Tag


class AuthorSerializer(serializers.ModelSerializer):
    # id = serializers.IntegerField()
    # name = serializers.CharField()

    class Meta:
        model = Author
        fields = "__all__"


class GenreSerializer(serializers.ModelSerializer):
    # id = serializers.IntegerField()
    # name = serializers.CharField()
    class Meta:
        model = Genre
        fields = "__all__"


class TagsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Tag
        fields = "__all__"


class TrackSerializer(serializers.ModelSerializer):
    # author = AuthorSerializer(read_only=True)
    # author = serializers.HyperlinkedRelatedField(view_name='author-list', read_only=True)
    # genre = GenreSerializer(write_only=True)
    pegi = serializers.SerializerMethodField()
    # tags = TagsSerializer(many=True)
    # tags = serializers.SerializerMethodField(required=False)

    class Meta:
        model = Track
        fields = "__all__"

    # track_name = serializers.CharField(source='name')
    # duration = serializers.DurationField()
    # pegi = serializers.SerializerMethodField()
    #
    # author = AuthorSerializer()
    # track_genre = GenreSerializer(source='genre')
    #
    def get_pegi(self, obj):
        return obj.get_pegi_display()

    # def get_tags(self, obj):
    #     return [x.name for x in obj.tags.all()]

    # def update(self, instance, validated_data):
    #     pass

    # def validate_pegi(self, validated_data):
    #     pass

    def create(self, validated_data):

        genre_serializer = GenreSerializer(data=validated_data.pop('genre'))
        if genre_serializer.is_valid():
            genre_serializer.save()
        else:
            serializers.ValidationError('Shoto poshlo ne tak')

        return super(TrackSerializer, self).create(validated_data)
