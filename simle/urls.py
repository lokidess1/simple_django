"""simle URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import debug_toolbar

# DO NOT DO THAT!!!!!
# import simle.settings as settings
# IMPORT SETTINGS ONLY LIKE THIS
from django.conf import settings

from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from api.views import TrackViewSet, AuthorViewSet
from rest_framework.routers import SimpleRouter
from graphene_django.views import GraphQLView


router = SimpleRouter()
router.register(r'tracks', TrackViewSet)
router.register(r'authors', AuthorViewSet)


urlpatterns = [
    # path('grappelli/', include('grappelli.urls')), # grappelli URLS
    path('admin/', admin.site.urls),
    path('__debug__/', include(debug_toolbar.urls)),
    path('', include(("core.urls", "core"), namespace='track')),
    path('api-auth/', include('rest_framework.urls')),
    path('api/v1/', include((router.urls, "api"), namespace='api')),
    path('api/v1/authors-list', AuthorViewSet.as_view({'get': 'list'}), name='author-list'),
    # path('api/v1/tracks/', TrackViewSet.as_view({'get': 'list'}))
    path("graphql", GraphQLView.as_view(graphiql=True)),
] + static(
    settings.STATIC_URL, document_root=settings.STATIC_ROOT
)
