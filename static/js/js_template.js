// $('#red-rectangle').click(function (e) {
//     // $(this).toggleClass('green');
//     // $(this).animate({width: 500, height: 500}, 1000);
//     $('.btn-primary').show();
// });
//
// $('.btn-primary').click(function (e) {
//     $(this).hide();
// });


// document.getElementById('red-rectangle').onclick = function (e) {
//     if(this.classList.contains('green')) {
//         this.classList.remove('green');
//     } else {
//         this.classList.add('green');
//     }
// };
// let btnNum = 0;
// $('#add-button').click(function (e) {
//     let button = $('<button class="btn btn-danger new-btn">New button ' + btnNum + '!</button>');
//
//     // button.click(function (e) {
//     //     $(this).remove();
//     // });
//
//     $('#buttons').append(button);
//     btnNum ++;
// });
//
// $('body').on('click', '.new-btn', function(e) {
//     $(this).remove();
// });

// $('.new-btn').click(function (e) {
//
// });

$('.del-btn').click(async function (e) {
    let id = $(this).data('id');
    await fetch('http://127.0.0.1:8000/api/v1/tracks/'+ id +'/', {
        method: 'DELETE'
    });

    $(this).closest('tr').remove();
});


$('.edit-btn').click(async function (e) {
    let id = $(this).data('id');
    let modal = $('#edit-modal');
    let response = await fetch('http://127.0.0.1:8000/js-track-form/'+ id +'/');
    response = await response.text();
    modal.find('.modal-body').html(response);
    modal.modal('show');
});

$('#save-update').click(function (e) {
    $('#edit-track-form').submit();
});