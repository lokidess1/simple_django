from unittest import TestCase
from unittest.mock import patch, Mock, mock_open

from some_to_test import obtain_random_skill, spell_list, some, splitter



class MagicTestCase(TestCase):

    # def setUp(self):
    #     self.player = Mock(skills=set())

    @patch('some_to_test.some_other_method', return_value=True)
    @patch('some_to_test.random.choices', return_value=['test'])
    def test_obtain_random_skill(self, mock_choices, mock_some_other_method):
        expected_value = 'test'
        value = obtain_random_skill()
        mock_choices.assert_called_once()
        mock_choices.assert_called_once_with(spell_list)
        # self.assertEqual(mock_some_other_method.call_count, 5)
        self.assertEqual(expected_value, value)

    @patch('some_to_test.random')
    def test_magic(self, mock_random):

        mock_random.choices = Mock(return_value=['test'])
        mock_random.randint = Mock(return_value=60)
        player = Mock(skills=set())
        # print(mock_random.sdfsdfsdf.asdasdsadasd.asdasdasd)

        expected_value = 'test'
        obtain_random_skill(player)

        mock_random.choices.assert_called_once()
        mock_random.randint.assert_called_once()

        self.assertIn(expected_value, player.skills)

    @patch('some_to_test.random.randint', return_value=10)
    def test_raises(self, mock_choice):
        player = Mock(skills=set())

        with self.assertRaises(ValueError):
            obtain_random_skill(player)

    @patch('some_to_test.findall', return_value=['test'])
    @patch('builtins.open', new_callable=mock_open, read_data='test')
    def test_some(self, mock_open_file, mock_findall):
        expected_value = ['test']

        value = some('test.txt')

        mock_open_file.assert_called_once_with('test.txt', 'r')
        mock_findall.assert_called_once_with('\d+', 'test')

        self.assertEqual(expected_value, value)

    # @patch('builtins.split', return_value=['q', 'w', 'e'])
    # def test_splitter(self, mock_splitter):
    #     value = splitter('qwe')
    #     print(value)
