import csv
import codecs
from datetime import timedelta
from django import forms
from core.models import Author, Genre, Tag, Track
from core.fields import PhoneField
from core.utils import notify
from django.contrib.auth import get_user_model
from django.template.loader import render_to_string
from django.conf import settings
from django.core.validators import RegexValidator


# class TrackForm(forms.Form):
#
#     name = forms.CharField(max_length=10)
#     duration = forms.DurationField()
#     author = forms.ModelChoiceField(queryset=Author.objects.all())
#     genre = forms.ModelChoiceField(
#         queryset=Genre.objects.all(),
#         widget=forms.widgets.RadioSelect()
#     )
#     tags = forms.ModelMultipleChoiceField(
#         queryset=Tag.objects.all(),
#         required=False,
#         widget=forms.widgets.CheckboxSelectMultiple()
#     )
#     # date = forms.DateTimeField(input_formats="")
#     # email = forms.EmailField()
#     # phone = forms.GenericIPAddressField
#
#     def clean_name(self):
#         if 'a' in self.cleaned_data['name']:
#             raise forms.ValidationError("You cannot use 'a' in name")
#         return self.cleaned_data['name']
#
#     def save(self, track_id=None):
#         tags = self.cleaned_data.pop('tags')
#         track = Track.objects.update_or_create(id=track_id, defaults=self.cleaned_data)[0]
#
#         # Track.objects.filter(id=track_id).update(**self.cleaned_data)
#
#         track.tags.set(tags)
#         return track
from core.widgets import FCKeditorWidget


class FormPasswordMixin:
    password = forms.PasswordInput()
    password_confirm = forms.PasswordInput()

    def clean_password_confirm(self):
        if self.cleaned_data['password'] != self.cleaned_data['password_confirm']:
            raise forms.ValidationError("Passwords do not match")
        return self.cleaned_data['password']


class PwdForm(FormPasswordMixin, forms.Form):
    pass


class TrackForm(forms.ModelForm):
    # phone = PhoneField()
    # description = forms.CharField(widget=FCKeditorWidget, validators=[RegexValidator('\w')])

    class Meta:
        model = Track
        # fields = ('name', 'duration', 'author')
        fields = '__all__'
        widgets = {
            'tags': forms.widgets.CheckboxSelectMultiple(),
            'genre': forms.widgets.RadioSelect()
        }

    def clean_name(self):
        if 'a' in self.cleaned_data['name']:
            raise forms.ValidationError("You cannot use 'a' in name")
        return self.cleaned_data['name']

    def save(self, track_id, commit=True):
        print(track_id)
        return super(TrackForm, self).save(commit)


class AuthorAdminForm(forms.ModelForm):

    class Meta:
        model = Author
        fields = '__all__'
        widgets = {
            'description': FCKeditorWidget()
        }


class RegistrationForm(forms.ModelForm):
    password_confirm = forms.CharField(widget=forms.widgets.PasswordInput())

    class Meta:
        model = get_user_model()
        fields = ('username', 'email', 'password', 'password_confirm')
        widgets = {
            'password': forms.widgets.PasswordInput()
        }

    def clean_password_confirm(self):
        if self.cleaned_data['qwe'] != self.cleaned_data['password']:
            raise forms.ValidationError("Passwords do not match")
        return self.cleaned_data['password']

    def save(self, commit=True):
        user = super(RegistrationForm, self).save(commit=False)
        # user.password = 'qweqweqwe' WILL NOT WORK!
        user.set_password(self.cleaned_data['password'])
        user.save()

        return user


class DockerComposeForm(forms.Form):
    command = forms.CharField()
    host_port = forms.IntegerField()
    container_port = forms.IntegerField()

    def save(self):
        data = render_to_string('docker-compose.yaml', context=self.cleaned_data)
        with open(settings.STATICFILES_DIRS[0] / 'new_compose.yaml', 'w') as yaml_file:
            yaml_file.write(data)


class TrackListUploadForm(forms.Form):
    track_list = forms.FileField()

    def save(self, **kwargs):
        data = list(csv.reader(codecs.iterdecode(self.cleaned_data['track_list'].readlines(), 'utf-8')))[1:]
        for row in data:
            Track.objects.create(
                name=row[0], author=Author.objects.filter(name=row[1]).first(),
                duration=timedelta(minutes=int(row[2].split(':')[0]), seconds=int(row[2].split(':')[1])),
                pegi=Track.PEGI_ALL, genre=Genre.objects.all().first()
            )

