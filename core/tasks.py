from django.core.mail import send_mail
from core.models import Subscribe
from simle import celery_app
from core.utils import get_privat_bank_currency


@celery_app.task
def send_mail_task(subject, text, email):
    # user = User.objects.filter(id=user_id).first()
    # if user:
    send_mail(subject=subject, message=text, recipient_list=[email], from_email='admin@example.com')
    print("Email was sent!")


@celery_app.task
def send_news_to_subscribers():
    for subscribe in Subscribe.objects.all():
        send_mail_task.delay('Asgard Times', 'Loki has stole some thing!', subscribe.email)


@celery_app.task
def store_currency():
    get_privat_bank_currency()
