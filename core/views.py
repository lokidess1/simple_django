import csv
from pprint import pprint

from django.views.generic import CreateView, UpdateView, ListView
from django.db.models import Q, Subquery, F
from django.shortcuts import redirect, get_object_or_404
from django.views.generic import FormView, CreateView
from django.views.generic.base import TemplateView, View
from django.db.models.aggregates import Count, Sum, Avg
from django.http.response import Http404
from django.urls.base import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http.response import JsonResponse
from django.contrib.auth.models import User
from django.db.models.functions import Lower

from core.models import Author, Track, Subscribe, Genre
from core.forms import TrackForm, RegistrationForm, AuthorAdminForm, DockerComposeForm, TrackListUploadForm
from core.tasks import send_mail_task
from django.http import HttpResponse
from api.serializers import AuthorSerializer


class IndexView(ListView):
    template_name = "index.html"
    paginate_by = 10
    model = Track
    context_object_name = 'tracks'

    def get_queryset(self):
        tracks = self.model.objects.all()
        if 'query' in self.request.GET:
            tracks = tracks.filter(
                Q(name__icontains=self.request.GET['query']) |
                Q(author__name__icontains=self.request.GET['query']) |
                Q(genre__name__icontains=self.request.GET['query'])
            )
        return tracks

    def get_context_data(self, *args, **kwargs):
        context = super(IndexView, self).get_context_data(*args, **kwargs)

        # context['my_lst'] = [1, 2, 3]
        # context['my_dict'] = {'name': 'Loki', 'age': 31}
        # context['count'] = int(self.request.GET.get('count', '5'))
        #
        # author = Author.objects.first()
        #
        # author_json = AuthorSerializer(author)
        # pprint(author_json.data)

        return context

    # @method_decorator(login_required)
    # def dispatch(self, request, *args, **kwargs):
    #     return super(IndexView, self).dispatch(request, *args, **kwargs)

    # def get_context_data(self, **kwargs):
    #     tracks = Track.objects.all()
    #
    #     context = {
    #         'greet': "Hello world!",
    #         'tracks': tracks
    #     }
    #
    #         # tracks.filter(**{'name': 'qwe'})
    #         context['tracks'] = tracks
    #
    #     # tracks = Track.objects.all().select_related(
    #     #     'author',
    #     #     'genre'
    #     # ).order_by('-genre__name')
    #
    #     # tracks = Track.objects.filter(name="Некромант")
    #     return context

    # def get(self, request, *args, **kwargs):
    #     pass

    # def post(self, request):
    #     pass


class NewIndex(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super(NewIndex, self).get_context_data(**kwargs)
        # tracks = Track.objects.all().select_related().prefetch_related('tags').values('id', 'name', 'duration')
        # # artists = Author.objects.all().prefetch_related('tracks')
        # artists = Author.objects.all().annotate(
        #     track_count=Count('tracks'),
        #     # all_duration=Sum('tracks__duration'),
        #     all_duration=Avg('tracks__duration')
        # ).filter(track_count__gt=0)
        #
        # context = {
        #     'greet': "Hello world!",
        #     'tracks': tracks,
        #     'artists': artists
        # }

        ganres = Genre.objects.all()[:3]

        context['tracks'] = Track.objects.filter(
            genre__pk__in=Subquery(ganres.values_list('id', flat=True))
        ).order_by("?").first()
        same = Track.objects.filter(author__name__contains=F('name'))

        return context


# class TrackUpdateView(TemplateView):
#     template_name = 'track_update.html'
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#
#         track = get_object_or_404(Track, id=self.kwargs['track_id'])
#
#         # if not Track.objects.filter(id=self.kwargs['track_id']).exists():
#         #     raise Http404()
#         # track = Track.objects.get(id=self.kwargs['track_id'])
#
#         # context['form'] = TrackForm(initial={
#         #     'name': track.name,
#         #     'duration': track.duration,
#         #     'author': track.author,
#         #     'genre': track.genre,
#         #     'tags': track.tags.all()
#         # })
#
#         context['form'] = TrackForm(instance=track)
#
#         return context
#
#     def post(self, request, track_id):
#         track = get_object_or_404(Track, id=track_id)
#
#         form = TrackForm(data=request.POST, instance=track)
#
#         if form.is_valid():
#             form.save()
#             return redirect('/')
#
#         return self.render_to_response({'form': form})


# class TrackCreateView(TemplateView):
#     template_name = 'create_track.html'
#
#     def get_context_data(self, **kwargs):
#         context = super(TrackCreateView, self).get_context_data(**kwargs)
#         context['form'] = TrackCreateForm()
#         return context
#
#     def post(self, request):
#         form = TrackCreateForm(data=request.POST)
#         if form.is_valid():
#             form.save()
#             return redirect('/')
#
#         return self.render_to_response({'form': form})


# class TrackUpdateView(FormView):
#     template_name = 'track_update.html'
#     form_class = TrackForm
#     success_url = '/'
#
#     def get_form_kwargs(self):
#         kwargs = super(TrackUpdateView, self).get_form_kwargs()
#         kwargs['instance'] = get_object_or_404(Track, id=self.kwargs['track_id'])
#         return kwargs
#
#     # def get_initial(self):
#     #     pass
#
#     def form_valid(self, form):
#         form.save()
#         return super(TrackUpdateView, self).form_valid(form)


# class TrackCreateView(FormView):
#     template_name = 'create_track.html'
#     form_class = TrackCreateForm
#     success_url = '/'
#
#     def form_valid(self, form):
#         form.save()
#         return super(TrackCreateView, self).form_valid(form)
#
#     def form_invalid(self, form):
#         pass


class TrackCreateView(CreateView):
    template_name = 'create_track.html'
    success_url = reverse_lazy('track:home')
    model = Track
    form_class = TrackForm


class TrackUpdateView(UpdateView):
    template_name = 'track_update.html'
    success_url = '/js-test/'  # reverse_lazy('track:home')
    model = Track
    # fields = '__all__'
    form_class = TrackForm
    pk_url_kwarg = 'track_id'

    def get_context_data(self, **kwargs):
        context = super(TrackUpdateView, self).get_context_data()
        context['tracks'] = Track.objects.all()
        return context

    def form_valid(self, form):
        form.save(track_id=self.kwargs['track_id'])
        return super(TrackUpdateView, self).form_valid(form)


class SubscribeView(CreateView):
    template_name = 'subscribe.html'
    model = Subscribe
    fields = '__all__'
    success_url = reverse_lazy('core:subscribe')
    
    def form_valid(self, form):
        response = super(SubscribeView, self).form_valid(form)
        send_mail_task.delay('You now subscribed', 'Some test text', form.cleaned_data['email'])
        return response


class JsonView(View):

    def get(self, request):
        return JsonResponse({
            'name': 'Loki',
            'age': 30,
            'cats': ['Dolka', 'Kivi']
        })


class RegistrationView(CreateView):
    model = User
    form_class = RegistrationForm
    template_name = 'registration.html'
    success_url = reverse_lazy('core:home')


class CreateAuthorView(CreateView):
    template_name = "create_author.html"
    model = Author
    fields = '__all__'
    success_url = '/'
#
#
# class UpdateAuthorView(UpdateView):
#     template_name = "create_author.html"
#     model = Author
#     fields = '__all__'
#     success_url = '/'


class DockerComposeGenerator(FormView):
    template_name = 'docker.html'
    form_class = DockerComposeForm
    success_url = '/static/new_compose.yaml'
    
    def dispatch(self, request, *args, **kwargs):
        # if 'Учитель' not in request.user.groups.all().values_list('name', flat=True):
        #     return redirect('/')
        return super(DockerComposeGenerator, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        form.save()
        return super(DockerComposeGenerator, self).form_valid(form)


class DownloadTrackList(View):

    def get(self, request):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="somefilename.csv"'

        writer = csv.writer(response)
        writer.writerow(['name', 'author', 'duration'])
        for track in Track.objects.all():
            writer.writerow([track.name, track.author, track.duration])
        return response


class UploadTrackList(FormView):
    form_class = TrackListUploadForm
    template_name = 'create_track.html'
    success_url = '/'

    def form_valid(self, form):
        form.save()
        return super(UploadTrackList, self).form_valid(form)


class JsView(ListView):
    template_name = 'js_template.html'
    model = Track
    context_object_name = 'tracks'

    def get_context_data(self, *args, **kwargs):
        context = super(JsView, self).get_context_data(*args, **kwargs)
        context['form'] = TrackForm()
        return context


class UpdateFormAjax(UpdateView):
    template_name = 'create_track.html'
    model = Track
    success_url = '/'
    form_class = TrackForm
    context_object_name = 'track'
