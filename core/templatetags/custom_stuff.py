from django import template
from core.models import Track
from core.forms import AuthorAdminForm

register = template.Library()


@register.filter
def dir_filter(var):
    return str(dir(var))


@register.filter
def multiply(first, second):
    return first * second


@register.simple_tag
def last_track():
    return Track.objects.all().last()


@register.inclusion_tag(filename='includes/track_table.html')
def last_5_tracks(count=5):
    return {
        'tracks': Track.objects.all()
    }


@register.inclusion_tag(filename='create_author.html')
def create_author_form():
    return {
        "form": AuthorAdminForm()
    }
