from django.contrib import admin
from django.utils.safestring import mark_safe
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy as _

from core.models import Author, Genre, Track, Tag, Subscribe, PlayList, MyUser, Currency

from core.forms import AuthorAdminForm
from django_extensions.admin import ForeignKeyAutocompleteAdmin


class AuthorAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'show_description')
    search_fields = ('name', 'description')

    form = AuthorAdminForm

    def show_description(self, obj):
        return mark_safe(obj.description)[:256] or ''
    show_description.short_description = "Description"
    show_description.admin_order_field = 'description'


class TrackAdmin(ForeignKeyAutocompleteAdmin):
    list_filter = ('tags', )

    # related_search_fields = {
    #     'author': ('name', )
    # }


class TrackInline(admin.TabularInline):
    model = Track
    extra = 1


class GenreAdmin(admin.ModelAdmin):
    inlines = [TrackInline]
    list_display = ('id', 'name', )
    list_editable = ('name', )


class MyUserAdmin(UserAdmin):
    fieldsets = (
        (None, {
            'fields': ('username', 'password')}),
        (_('Personal info'), {
            'fields': ('first_name', 'last_name', 'email', 'age')}),
        (_('Permissions'), {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions'),
        }),
        (_('Important dates'), {
            'fields': ('last_login', 'date_joined')}),
    )


class CurrencyAdmin(admin.ModelAdmin):
    list_display = ('ccy', 'buy_price', 'sell_price', 'created')


admin.site.register(Author, AuthorAdmin)
admin.site.register(Genre, GenreAdmin)
admin.site.register(Track, TrackAdmin)
admin.site.register(PlayList)
admin.site.register(Tag)
admin.site.register(Subscribe)
admin.site.register(MyUser, MyUserAdmin)
admin.site.register(Currency, CurrencyAdmin)
