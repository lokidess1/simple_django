from django.urls import path
from core.views import IndexView, NewIndex, TrackCreateView, TrackUpdateView, SubscribeView, JsonView, \
    RegistrationView, CreateAuthorView, DockerComposeGenerator, DownloadTrackList, UploadTrackList, JsView, \
    UpdateFormAjax


urlpatterns = [
    path('', IndexView.as_view(), name='home'),
    path('new/', NewIndex.as_view()),
    path('create/', TrackCreateView.as_view(), name='create'),
    path('update/<int:track_id>/', TrackUpdateView.as_view(), name='update'),
    path('subscribe/', SubscribeView.as_view(), name='subscribe'),
    path('api/v1/dummy/', JsonView.as_view(), name='dummy_appi'),
    path('registration/', RegistrationView.as_view(), name='registration'),
    path('create/author/', CreateAuthorView.as_view(), name='create_author'),
    path('docker/', DockerComposeGenerator.as_view(), name='docker'),
    path('download/', DownloadTrackList.as_view(), name='download'),
    path('upload/', UploadTrackList.as_view(), name='upload'),
    path('js-test/', JsView.as_view(), name='js-test'),
    path('js-track-form/<int:pk>/', UpdateFormAjax.as_view())
]
