from pprint import pprint
import requests
import json
from lxml import html
from core.models import Currency


def notify(text):
    # bot_token = os.environ.get("BOT_TOKEN")
    # chat_id = os.environ.get("BOT_CHAT")
    #
    # url = f"https://api.telegram.org/bot{bot_token}/sendMessage"
    #
    # requests.get(url, data={'chat_id': chat_id, 'text': text})
    print(f"Notify {text}")


def get_privat_bank_currency():
    response = requests.get('https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5')
    if response.status_code == 200:
        # data = json.loads(response.content)
        data = response.json()
        currency_lst = []

        for row in data:
            ccy = Currency.convert_str_to_choice(row['ccy'])
            if ccy:
                currency_lst.append(Currency(
                    ccy=ccy,
                    buy_price=row['buy'],
                    sell_price=row['sale']
                ))

        Currency.objects.bulk_create(currency_lst)
    else:
        print(response.status_code)


def get_ukrsib_bank_currency():
    headers = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'
    }
    response = requests.get('https://my.ukrsibbank.com/ru/personal/', headers=headers)
    if response.status_code == 200:
        tree = html.fromstring(response.text)
        currency_map = [{
            'ccy': 'USD',
            'buy': '//*[@id="NALUSD"]/div[1]/p',
            'sale': '//*[@id="NALUSD"]/div[2]/p'
        }, {
            'ccy': 'EUR',
            'buy': '//*[@id="NALEUR"]/div[1]/p',
            'sale': '//*[@id="NALEUR"]/div[2]/p'
        }, {
            'ccy': 'RUR',
            'buy': '//*[@id="NALRUB"]/div[1]/p',
            'sale': '//*[@id="NALRUB"]/div[2]/p'
        }]

        currency_lst = []

        for row in currency_map:
            ccy = Currency.convert_str_to_choice(row['ccy'])
            if ccy:
                currency_lst.append(Currency(
                    ccy=ccy,
                    buy_price=tree.xpath(row['buy'])[0].text,
                    sell_price=tree.xpath(row['sale'])[0].text
                ))

        Currency.objects.bulk_create(currency_lst)
