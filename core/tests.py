from django.urls.base import reverse_lazy
from django.test import TestCase, Client
from core.models import Track, Author, Genre
import datetime


class DiagnosticTestCase(TestCase):

    # def setUp(self):
    #     pass
    #
    # def setUpClass(cls):
    #     pass
    #
    # def tearDown(self):
    #     pass
    #
    # def tearDownClass(cls):
    #     pass

    def setUp(self):
        user = User.objects.create(username='admin', email="qqwe@awe.com")
        user.set_password('admin')
        user.save()

        self.client = Client()

    def test_index_page(self):
        # response = requests.get('http://127.0.0.1:8000/')
        self.client.login(username='admin', password="admin")
        response = self.client.get(reverse_lazy('core:home'))
        # assert response.status_code == 200
        self.assertIn('user', response.context)
        self.assertEqual(response.status_code, 200)

    def test_index_track_count(self):
        author = Author.objects.create(name='Test Author')
        genre = Genre.objects.create(name='Test Genre')
        for counter in range(10):
            Track.objects.create(
                duration=datetime.timedelta(days=-1, seconds=68400),
                pegi=Track.PEGI_18_PLUS,
                author=author,
                genre=genre
            )
        response = self.client.get(reverse_lazy('core:home'))
        self.assertEqual(len(response.context['tracks']), 10)

    def test_index_track_name_filter(self):
        author = Author.objects.create(name='Test Author')
        genre = Genre.objects.create(name='Test Genre')
        for counter in range(10):
            Track.objects.create(
                duration=datetime.timedelta(days=-1, seconds=68400),
                pegi=Track.PEGI_18_PLUS,
                author=author,
                genre=genre,
                name='test to find'
            )

        Track.objects.create(
            duration=datetime.timedelta(days=-1, seconds=68400),
            pegi=Track.PEGI_18_PLUS,
            author=author,
            genre=genre
        )

        response = self.client.get(reverse_lazy('core:home'), data={'query': 'test to find'})
        self.assertEqual(len(response.context['tracks']), 10)
