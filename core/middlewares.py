from django.utils.deprecation import MiddlewareMixin
from django.utils.functional import SimpleLazyObject
from django.shortcuts import redirect
from django.conf import settings


class LoginRequiredMiddleware(MiddlewareMixin):

    def process_request(self, request):
        if not request.user.is_authenticated and request.path in settings.NOT_LOGIN_REQUIRED_URLS:
            return redirect('/admin/login/')

    def process_response(self, request, response):
        response.content = response.content.replace(b"<body>", b"<body><h1>Hello world</h1>")
        return response


class AuthenticationMiddleware(MiddlewareMixin):
    def process_request(self, request):
        assert hasattr(request, 'session'), (
            "The Django authentication middleware requires session middleware "
            "to be installed. Edit your MIDDLEWARE setting to insert "
            "'django.contrib.sessions.middleware.SessionMiddleware' before "
            "'django.contrib.auth.middleware.AuthenticationMiddleware'."
        )
        request.user = SimpleLazyObject(lambda: get_user(request))
