from django.core.management.base import BaseCommand
from core.utils import get_ukrsib_bank_currency


class Command(BaseCommand):

    def handle(self, *args, **options):
        get_ukrsib_bank_currency()
