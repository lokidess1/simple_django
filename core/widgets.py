from django.forms import widgets


class FCKeditorWidget(widgets.Widget):
    template_name = 'widgets/fckeditor.html'
