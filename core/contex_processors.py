from django.conf import settings


def settings_context_processor(request):
    return {'settings': settings}


def select_related_for_users(request):
    return {
        'user': request.user.select_related().prefech_related('groups')
    }
