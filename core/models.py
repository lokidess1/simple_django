from random import randint
from django.db import models
from django.contrib.auth.models import User, AbstractUser
from django.contrib.auth import get_user_model
from django.db.models.aggregates import Max


# class Profile(models.Model):
#     age = models.IntegerField()
#     user = models.OneToOneField(User, on_delete=models.CASCADE)


class MyUser(AbstractUser):
    age = models.IntegerField(default=0)


class Subscribe(models.Model):
    email = models.EmailField()

    def __str__(self):
        return self.email


class Author(models.Model):
    name = models.CharField(max_length=255, verbose_name="Имя")
    description = models.TextField(blank=True, null=True)

    class Meta:
        verbose_name = "Афтор"
        verbose_name_plural = "АфторЫ"

    def __str__(self):
        return self.name


class PlayList(models.Model):
    name = models.CharField(max_length=255)
    tracks = models.ManyToManyField("core.Track", blank=True)
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Genre(models.Model):
    name = models.CharField(max_length=255)
    # tags = models.ManyToManyField('core.Tags', blank=True)

    def __str__(self):
        return self.name


class Tag(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Track(models.Model):

    PEGI_18_PLUS = 0
    PEGI_ALL = 1

    PEGI_CHOICES = (
        (PEGI_18_PLUS, '18+'),
        (PEGI_ALL, 'All')
    )

    name = models.CharField(max_length=255)
    duration = models.DurationField()
    author = models.ForeignKey(Author, on_delete=models.CASCADE, related_name='tracks')
    genre = models.ForeignKey(Genre, on_delete=models.CASCADE)
    tags = models.ManyToManyField("core.Tag", blank=True)
    pegi = models.PositiveSmallIntegerField(choices=PEGI_CHOICES)

    def __str__(self):
        return self.name

    def get_random_track(self):
        max_id = self.__class__.objects.aggregate(max_id=Max('id'))["max_id"]
        while True:
            obj = self.__class__.objects.filter(id=randint(1, max_id)).first()
            if obj:
                return obj

    def save(self, **kwargs):
        # PRE SAVE
        resp = super(Track, self).save(**kwargs)
        # POST SAVE
        return resp


class Currency(models.Model):
    CCY_USD = 1
    CCY_EUR = 2
    CCY_RUR = 3

    CCY_CHOICES = (
        (CCY_USD, 'USD'),
        (CCY_EUR, 'EUR'),
        (CCY_RUR, 'RUR')
    )

    ccy = models.PositiveSmallIntegerField(choices=CCY_CHOICES)
    buy_price = models.DecimalField(max_digits=10, decimal_places=2)
    sell_price = models.DecimalField(max_digits=10, decimal_places=2)
    created = models.DateTimeField(auto_now_add=True)

    @classmethod
    def convert_str_to_choice(cls, choice_name):
        choices = dict(map(reversed, cls.CCY_CHOICES))
        if choice_name in choices:
            return choices[choice_name]


from django.db.models.signals import post_save, pre_save
from core.models import Track


def send_notify(**kwargs):
    from core.utils import notify
    notify('Track was added')


def evil_signal(instance, **kwargs):
    instance.name = "HAHAHAHAH! EVIL!"


post_save.connect(send_notify, sender=Track)
# pre_save.connect(evil_signal, sender=Track)
